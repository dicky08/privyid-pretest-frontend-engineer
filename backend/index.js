const express = require('express')
const app = express()
const bodyParser = require("body-parser")
const userRegisterRoute = require('./src/routes/users')
const cors = require("cors")
const {
  PORT
} = require("./src/helper/env")
const {
  static
} = require("express")
app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(express.static("src/img"))
app.use('/api/v1', userRegisterRoute)
app.listen(PORT, () => {
  console.log(`run PORT ${PORT}`)
})