const {registrasiModel, sendOTPModel, getAllModel, requestOTP,matchOTP, signInModel} = require('../model/userModel')
const bcrypt = require("bcrypt");
const userController = {
  getAllCtr: async (req,res) => {
    const result = await getAllModel() 
    res.json({
      msg: 'Get all success',
      data: result
    })
  },
  regisCtr: async (req, res) => {
    const body = req.body
    let numbRandom = ''
    for (let i = 1; i <= 4; i++) {
      let x = Math.floor(Math.random()*5 + 4);
      numbRandom += x
    }
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(body.password, salt);
    const dataRegis = {
      phone: body.phone,
      password: hash,
      country: body.country
    }
    try {
      const result = await registrasiModel(dataRegis)
      await sendOTPModel(result.insertId,numbRandom)
        res.json({
          id: result.insertId,
          status: 200,
          phone: dataRegis.phone,
          kd_otp: numbRandom,
          msg: 'Succes Registrasi'
        })
    } catch (error) {
        console.log(error.message);
    }
  },
  requestOTP: async (req,res) => {
    const body = req.body
    let numbRandom = ''
    for (let i = 1; i <= 4; i++) {
      let x = Math.floor(Math.random()*5 + 4);
      numbRandom += x
    }
    try {
      const dataRegister = await getAllModel()
      dataRegister.forEach(async e => {
        if (e.phone === body.phone) {
          const data = {
            id: e.id,
            kd_otp: numbRandom
          }
            await requestOTP(data)
            res.json({
              msg: 'Request OTP success',
              data: data.kd_otp
            })
          }else {
            return false
          }
      })
    } catch (error) {
      console.log(error.message);
    }
  },
  matchOTPCtr: async (req,res) => {
    const {kode_otp} = req.body
    try {
      const result = await matchOTP(kode_otp)
      if (result.length===1) {
        res.json({
          msg: 'Success OTP match',
          data: result
        })
      } else {
        res.json({
          msg: 'Kode OTP not match',
          data: result
        })
      }
     
    } catch (error) {
      console.log(error.message);
    }
  },
  signInCtr:  (req,res) => {
    const body = req.body
     signInModel(body)
     .then(async (result) => {
       const dataUser= result[0]
       if (dataUser) {
         const pwDB = dataUser.password
         const match = await bcrypt.compare(body.password, pwDB)
         if (match) {
           res.json({
            id: dataUser.id, 
            msg: 'Login success',
           })
         } else {
           res.json({
             msg: 'Wrong password!'
           })
         }
       } else {
         res.json({
           msg: 'Data not found'
         })
       }
     })
    .catch((error) => {
      console.log(error.message);
    })
  }
}

module.exports = userController