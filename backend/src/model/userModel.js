// Import Database
const db = require("../config/db_privy")

const userModel = {
  getAllModel: () => {
    return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM user JOIN otp ON user.id=otp.id`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  registrasiModel: (data) => {
    return new Promise((resolve, reject) => {
      db.query(
        `INSERT INTO user (phone,password,country)
            VALUES('${data.phone}', '${data.password}', '${data.country}')`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  sendOTPModel: (id, data) => {
    return new Promise((resolve, reject) => {
      db.query(
        `INSERT INTO otp (id,kode_otp)
            VALUES('${id}','${data}')`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  requestOTP: (data) => {
    return new Promise((resolve, reject) => {
      db.query(
        `UPDATE otp SET
          kode_otp='${data.kd_otp}'
          WHERE id='${data.id}'`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  matchOTP: (kd_otp) => {
    return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM otp WHERE kode_otp='${kd_otp}'`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  signInModel: (data) => {
    return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM user WHERE phone='${data.phone}'`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  updateModel: (data, id) => {
    return new Promise((resolve, reject) => {
      db.query(
        `UPDATE category SET 
            category_name='${data.category_name}' 
            WHERE id='${id}'`,
        (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        }
      );
    });
  },
  deleteModel: (id) => {
    return new Promise((resolve, reject) => {
      db.query(`DELETE FROM category WHERE id ='${id}'`, (err, result) => {
        if (err) {
          reject(new Error(err));
        } else {
          resolve(result);
        }
      });
    });
  },
};

module.exports = userModel;
