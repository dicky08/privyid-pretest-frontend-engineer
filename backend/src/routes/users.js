const express = require("express");
const router = express.Router();

const {regisCtr,requestOTP,getAllCtr,matchOTPCtr, signInCtr} = require('../controller/userController');
router
.post('/register',regisCtr)
.post('/register/otp/request',requestOTP)
.get('/register/getAll',getAllCtr)
.post('/register/otp/match',matchOTPCtr)
.post('/oauth/sign_in',signInCtr)

module.exports = router;