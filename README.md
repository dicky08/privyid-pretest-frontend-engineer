Ini merupakan aplikasi sederhana yang dibangun menggunakan framework Node+ExpressJS untuk restfull API.
Antarmuka dibangun menggunkan Vue JS. Didalam aplikasi ini securty password menggunakan bcrypt serta menggunakan axios
untuk get/post API.
Langkah-langkah:
1. User melakukan registrasi dengan memasukan Phone number, password dan country
2. Setelah memasukan semua data. User dialihkan ke halaman OTP dimana user akan memasukan 
OTP yang telah dikirim saat pertama kali mendaftar.
3. User juga dapat langsung mereset/meminta ulang kode OTP
4. Setelah selesai user akan dialihkan ke halaman login
5. User dapat melakukan login dan dialihkan ke dalam halaman profile.
6. User dapat melakukan Logout