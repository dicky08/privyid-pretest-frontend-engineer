import axios from 'axios'
const state = () => {
  return {
    users: {
      register: []
    }
  }
}
const getters = {
  registerUser (state) {
    return state.users
  }
}
const mutations = {
  SET_DATA_USERS (state, payload) {
    state.users = payload
  }
}
const actions = {
  register (context, payload) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/api/v1/register', payload)
        .then((result) => {
          resolve(result.data)
        }).catch((err) => {
          console.log(err.message)
        })
    })
  },
  verifikasiOTP (context, payload) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/api/v1/register/otp/match', payload)
        .then((result) => {
          resolve(result.data)
        }).catch((err) => {
          console.log(err.message)
        })
    })
  },
  sendNewKdOTP (context, payload) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/api/v1/register/otp/request', payload)
        .then((result) => {
          resolve(result.data)
        }).catch((err) => {
          console.log(err.message)
        })
    })
  },
  signIn (context, payload) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/api/v1/oauth/sign_in', payload)
        .then((result) => {
          localStorage.setItem('id', result.data.id)
          resolve(result.data)
        }).catch((err) => {
          console.log(err.message)
        })
    })
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
